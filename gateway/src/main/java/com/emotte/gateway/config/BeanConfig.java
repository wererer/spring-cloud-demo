package com.emotte.gateway.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Mono;

@Configuration
public class BeanConfig {
	/**
	 * ip 进行限流
	 * @return
	 */
	@Bean
	public KeyResolver ipKeyResolver() {
		return exchange -> Mono.just(exchange.getRequest().getRemoteAddress().getHostName());
	}
	/**
	 * 用户限流
	 */
	@Bean
	public  KeyResolver userKeyResolver(){
		return  exchange -> Mono.just(exchange.getRequest().getQueryParams().getFirst("userId"));
	}
	/**
	 * 接口限流
	 */
}
