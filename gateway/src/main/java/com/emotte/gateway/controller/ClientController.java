package com.emotte.gateway.controller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ClientController {



    @RequestMapping("/gateway")
    public String hello() {
        return "hello gateway";
    }
}
