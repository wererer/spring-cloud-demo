package com.emotte.eurekaserver;

import com.emotte.eurekaserver.entity.ServerManagerLog;
import com.emotte.eurekaserver.mapper.ServerManagerLogsMapper;
import com.netflix.appinfo.InstanceInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.eureka.server.event.*;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EurekaStateChangeListener {

	@Autowired
	private ServerManagerLogsMapper serverManagerLogsMapper;

   /*
    注册到eureka的客户端监听管理
    */
	@EventListener
	public void listen(EurekaInstanceCanceledEvent event) {
		ServerManagerLog serverManagerLog = new ServerManagerLog(event.getAppName(),"服务下线",event.getServerId());
		insert(serverManagerLog);
		System.err.println(event.getServerId() + "\t" + event.getAppName() + " 服务下线");
		log.info(event.getServerId() + "\t" + event.getAppName() + " 服务下线");
	}

	@EventListener
	public void listen(EurekaInstanceRegisteredEvent event) {
		InstanceInfo instanceInfo = event.getInstanceInfo();
        instanceInfo.getHostName();
		ServerManagerLog serverManagerLog = new ServerManagerLog(instanceInfo.getAppName(),"进行注册",instanceInfo.getHostName());
		insert(serverManagerLog);
        System.err.println("name:"+instanceInfo.getAppName());
        System.err.println(instanceInfo.getAppName() + "进行注册");
		log.info(instanceInfo.getAppName() + "进行注册");
	}

	@EventListener
	public void listen(EurekaInstanceRenewedEvent event) {
//		ServerManagerLog serverManagerLog = new ServerManagerLog(event.getAppName(),"服务进行续约",event.getServerId());
//		insert(serverManagerLog);
		System.err.println(event.getServerId() + "\t" + event.getAppName() + " 服务进行续约");
		log.info(event.getServerId() + "\t" + event.getAppName() + " 服务进行续约");
	}

	@EventListener
	public void listen(EurekaRegistryAvailableEvent event) {
		ServerManagerLog serverManagerLog = new ServerManagerLog("","注册中心启动","");
		insert(serverManagerLog);
		System.err.println("注册中心 启动");
		log.info("注册中心 启动");
	}

	@EventListener
	public void listen(EurekaServerStartedEvent event) {
		ServerManagerLog serverManagerLog = new ServerManagerLog("","Eureka Server 启动","");
		insert(serverManagerLog);
		System.err.println("Eureka Server 启动");
		log.info("Eureka Server 启动");
	}


	private    void  insert(ServerManagerLog serverManagerLog){
		serverManagerLogsMapper.save(serverManagerLog);
	}
}
