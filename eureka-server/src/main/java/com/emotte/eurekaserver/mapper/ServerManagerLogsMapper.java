package com.emotte.eurekaserver.mapper;

import com.emotte.eurekaserver.entity.ServerManagerLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Classname ServerManagerLogMapper
 * @Description mapper
 * @Date 2020/4/25 18:33
 * @Created by sunyulong
 */
@Repository
public interface ServerManagerLogsMapper extends JpaRepository<ServerManagerLog,Long> {

}
