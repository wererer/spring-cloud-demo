package com.emotte.eurekaserver.entity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @Classname ServerManagerLog
 * @Description 服务管理日志
 * @Date 2020/4/25 18:30
 * @Created by sunyulong
 */
@EqualsAndHashCode(callSuper=true)
@Data
@Builder
@Entity
@Table(name = "server_manager_log")
@ToString(callSuper = true)
public class ServerManagerLog extends AbstractAuditModel{
    public  String name; //系统名

    public  String message;//消息

    @Column(name = "server_id")
    public  String serverId;//服务id

    public  ServerManagerLog( ){
    }
    public  ServerManagerLog(String name,String message,String serverId){
        this.name = name;
        this.message = message;
        this.serverId = serverId;
    }
    public  ServerManagerLog( String message ){
        this.message = message;
    }

}
