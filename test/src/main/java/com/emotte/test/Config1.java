package com.emotte.test;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p> Description: 类描述</p>
 *
 * @Project eureka-server02
 * @Author: TF
 * @CreateDate: 2019/5/31 17:28
 * @UpdateUser: TF
 * @UpdateDate: 2019/5/31 17:28
 * @Version: 版本
 */
@Configuration
@ConditionalOnClass(name="com.emotte.test.SunyuLong")
public class Config1 {

    @Bean
    public ZhangSan zs(){
        return new ZhangSan();
    }
}
