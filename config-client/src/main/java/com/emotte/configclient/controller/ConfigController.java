package com.emotte.configclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> Description: 类描述</p>
 *
 * @Project eureka-server02
 * @Author: TF
 * @CreateDate: 2019/6/10 16:39
 * @UpdateUser: TF
 * @UpdateDate: 2019/6/10 16:39
 * @Version: 版本
 */

@RestController
@RefreshScope
public class ConfigController {
    @Value("${name}")
    String name;

    @RequestMapping(value = "/name")
    public String getName(){
        return  name;
    }
}
