package com.emotte.model;

/**
 * <p> Description: 类描述</p>
 *
 * @Project eureka-server02
 * @Author: TF
 * @CreateDate: 2019/5/29 16:26
 * @UpdateUser: TF
 * @UpdateDate: 2019/5/29 16:26
 * @Version: 版本
 */
public class UserVo {

    private String userId;

    private String userName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
