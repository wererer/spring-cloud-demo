package com.emotte.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
@SpringBootApplication
public class ZuulClientApplication {
// 指定环境（开发演示用，不能用于生产环境））
		//System.setProperty("env", "DEV");
    public static void main(String[] args) {
        SpringApplication.run(ZuulClientApplication.class, args);
    }


}
