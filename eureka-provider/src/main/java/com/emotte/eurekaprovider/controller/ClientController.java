package com.emotte.eurekaprovider.controller;
import com.emotte.eurekaprovider.service.impl.ClientServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.serviceregistry.Registration;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p> Description: 类描述</p>
 *
 * @Project eureka-server02
 * @Author: TF
 * @CreateDate: 2019/5/29 16:54
 * @UpdateUser: TF
 * @UpdateDate: 2019/5/29 16:54
 * @Version: 版本
 */
@RestController
@Slf4j
public class ClientController {

    @Autowired
    private Registration registration;

    @Autowired
    private DiscoveryClient client;

    @Autowired
    private ClientServiceImpl clientService;

    @Value("${server.port}")
    String port;

   /* @GetMapping("/add")
    public UserVo service(@RequestParam String userId){
        List<ServiceInstance> instances = client.getInstances(registration.getServiceId());
        if(instances != null && instances.size()>0){
            for (int i=0; i<instances.size();i++) {
                log.info("/add,host:" + instances.get(i).getHost() + ",service_id:" + instances.get(i).getInstanceId());
            }
        }
        UserVo userVo = clientService.addService(userId);
        return  userVo;
    }*/
   //http://localhost:8082/index
   @GetMapping("/index")
   public String index() {
       return "index";
   }

    @RequestMapping("/hello")
    public String hello(@RequestParam String name) {
        log.info("hello "+name+"，from "+ port+ " this is new world");
        return "hello "+name+"，from "+ port+ " this is new world";
    }

    @RequestMapping("/add")
    public String add(@RequestParam String name) {
        log.info("add "+name+"，from "+ port+ " this is new world");
        return "add "+name+"，from "+ port+ " this is new world";
    }
}
