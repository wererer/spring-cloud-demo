package com.emotte.eurekaprovider.service.impl;

import com.emotte.eurekaprovider.service.ClientService;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.FrameworkServlet;

/**
 * <p> Description: 类描述</p>
 *
 * @Project eureka-server02
 * @Author: TF
 * @CreateDate: 2019/6/4 11:00
 * @UpdateUser: TF
 * @UpdateDate: 2019/6/4 11:00
 * @Version: 版本
 */
@Service
@Slf4j
public class ClientServiceImpl implements ClientService {
    @Override
    public String addService(String userId){
        log.info("开始------------");
        return "userId:"+userId;
    }
}
