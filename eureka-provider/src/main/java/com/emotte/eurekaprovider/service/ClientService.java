package com.emotte.eurekaprovider.service;


/**
 * <p> Description: 类描述</p>
 *
 * @Project eureka-server02
 * @Author: TF
 * @CreateDate: 2019/6/4 10:59
 * @UpdateUser: TF
 * @UpdateDate: 2019/6/4 10:59
 * @Version: 版本
 */
public interface ClientService {

    String addService(String userId);
}
