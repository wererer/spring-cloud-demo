package com.emotte.eurekaclient.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class BeanConfiguration {

	@Bean
	@LoadBalanced  //将其交由ribbon管理，做成拦截器，并实现负载均衡等策略
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

}
