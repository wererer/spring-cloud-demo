package com.emotte.eurekaclient.controller;

import com.emotte.eurekaclient.service.ExampleService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * <p> Description: 类描述</p>
 *
 * @Project eureka-server02
 * @Author: TF
 * @CreateDate: 2019/5/30 14:50
 * @UpdateUser: TF
 * @UpdateDate: 2019/5/30 14:50
 * @Version: 版本
 */
@RestController
public class ConsumerController {

    @Autowired
    RestTemplate template;
    @Autowired
    ExampleService exampleService;

    @Value("${spring.application.name}")
    String applicationName;

    @RequestMapping(value = "/ribbon-consumer",method = RequestMethod.GET)
    @HystrixCommand(fallbackMethod = "clientRequestError")
    public String clientRequest(){
        return  template.getForEntity("http://product-server//add?userId={1}",String.class,"1").getBody();
    }
    //访问地址 http://localhost:8094/hello/123
    @GetMapping("/hello/{name}")
    @HystrixCommand(fallbackMethod = "clientRequestError")
    public String index(@PathVariable("name") String name) {
        return exampleService.hello(name+", from "+applicationName);
    }

    public String clientRequestError(){
        return "Hi,sorry, system error.";
    }

}
