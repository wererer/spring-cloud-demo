## 项目介绍
###spring-cloud-demo
#####1.eureka集群
服务注册与发现&&心跳与故障
#####2.feign 负载均衡
feign 是对一个接口打了一个注解，会针对这个注解标注的接口生成动态代理，
然后你针对feign的动态代理去调用他的方式的时候，此时已在底层生成http协议格式的请求
底层的话，使用http通信的框架组件，httpclient 先得使用Ribbon去从本地的Eureka注册表的缓存中获取对方机器的列表，然后进行负载均衡，选择一台出来，接着针对那台机器发送http请求就行

#####3.gateway/zuul 路由转发
配置一下不同的请求路径和服务的对应关系，你的请求到了网关，它直接查找到匹配的服务
然后就直接吧请求转发给那个服务的某台机器，Ribbon从Eureka本地缓存列表获取一台机器，
负载均衡，把请求直接用http通信框架发送到制定的机器上去
#####4.config 配置中心
#####5. zipkin 链路追踪
 * 构建zipkin-server 在spring Cloud为F版本的时候，已经不需要自己构建Zipkin Server了，只需要下载jar即可。
 
 * 下载地址：https://dl.bintray.com/openzipkin/maven/io/zipkin/java/zipkin-server/

* 官方提供了一键脚本（Windows下需要安装curl，不过如果你安装了Git客户端，可以直接在Git Bash中使用）
 curl -sSL https://zipkin.io/quickstart.sh | bash -s
* 启动脚本 java -jar zipkin.jar
* 依次启动项目
eureka-server
admin-server
eureka-provider
eureka-client-feign
gateway
* 输入链接
通过feign进行负载均衡访问：http://127.0.0.1:8095/hello/999
通过gateway进行路由转发：http://127.0.0.1:8072/feign-test/hello/999
* 链路追踪实例截图
![](.README_images/lianlu.png)




#####6. admin  监控

