package com.emotte.eurekaclientfeign.controller;

import com.emotte.eurekaclientfeign.feign.EurekaFeignHystrixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> Description: 类描述</p>
 *
 * @Project eureka-server02
 * @Author: TF
 * @CreateDate: 2019/6/4 16:20
 * @UpdateUser: TF
 * @UpdateDate: 2019/6/4 16:20
 * @Version: 版本
 */
@RestController
public class EurekaFeignController {

    @Autowired
    private EurekaFeignHystrixService eurekaFeignHystrixService;

//    @Autowired
//    private EurekaFeignFactoryService eurekaFeignFactoryService;

    @Value("${spring.application.name}")
    String applicationName;


//    @RequestMapping("/feignAdd/{name}")
//    public String feignAdd(@PathVariable("name") String name){
//        return "feign从服务提供者放获取消息"+eurekaFeignFactoryService.getAdd("eurekaFeignFactoryService"+name+", from "+applicationName);
//    }

    @GetMapping("/hello/{name}")
    public String index(@PathVariable("name") String name) {
        return eurekaFeignHystrixService.hello("eurekaFeignHystrixService"+name+", from "+applicationName);
    }
}
