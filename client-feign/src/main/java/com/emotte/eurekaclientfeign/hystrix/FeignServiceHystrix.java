package com.emotte.eurekaclientfeign.hystrix;
import com.emotte.eurekaclientfeign.feign.EurekaFeignHystrixService;
import org.springframework.stereotype.Component;
/**
 * <p> Description: 类描述</p>
 *
 * @Project eureka-server02
 * @Author: TF
 * @CreateDate: 2019/6/4 17:50
 * @UpdateUser: TF
 * @UpdateDate: 2019/6/4 17:50
 * @Version: 版本
 */
@Component
public class FeignServiceHystrix implements EurekaFeignHystrixService {
    @Override
    public String getAdd(String userId) {
        return "FeignServiceHystrix error";
    }
    @Override
    public String hello(String name) {
        return "FeignServiceHystrix sorry "+name+"，service has fail!";
    }

}
