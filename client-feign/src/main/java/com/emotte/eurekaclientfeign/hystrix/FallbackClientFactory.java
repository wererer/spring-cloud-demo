//package com.emotte.eurekaclientfeign.hystrix;
//import com.emotte.eurekaclientfeign.feign.EurekaFeignFactoryService;
//import feign.hystrix.FallbackFactory;
//import org.springframework.stereotype.Component;
///**
// * <p> Description: 类描述</p>
// *
// * @Project eureka-server02
// * @Author: TF
// * @CreateDate: 2019/6/4 20:58
// * @UpdateUser: TF
// * @UpdateDate: 2019/6/4 20:58
// * @Version: 版本
// */
//
//@Component
//public class FallbackClientFactory implements FallbackFactory<EurekaFeignFactoryService> {
//
//    @Override
//    public EurekaFeignFactoryService create(Throwable arg0) {
//        return new EurekaFeignFactoryService() {//这步可以换成直接new接口：new UserFeignClient()
//            @Override
//            public String getAdd(String userId) {
//                return "FallbackClientFactory error:"+userId;
//            }
//            @Override
//            public String hello( String name) {
//                return "FallbackClientFactory sorry" +name+"，service has fail!";
//            }
//        };
//    }
//}
//
