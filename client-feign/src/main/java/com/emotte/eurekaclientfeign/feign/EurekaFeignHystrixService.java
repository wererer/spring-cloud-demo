package com.emotte.eurekaclientfeign.feign;
import com.emotte.eurekaclientfeign.hystrix.FeignServiceHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "product-server"/*,fallbackFactory = FallbackClientFactory.class*/,fallback = FeignServiceHystrix.class)
public interface EurekaFeignHystrixService {
    @RequestMapping("/add")
    String getAdd(@RequestParam("userId") String userId);

    @GetMapping("hello")
    public String hello(@RequestParam(value = "name") String name);
}
