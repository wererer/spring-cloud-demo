//package com.emotte.eurekaclientfeign.feign;
//import com.emotte.eurekaclientfeign.hystrix.FallbackClientFactory;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//@FeignClient(value = "product-server",fallbackFactory = FallbackClientFactory.class)
//public interface EurekaFeignFactoryService {
//    @RequestMapping("/add")
//    String getAdd(@RequestParam("userId") String userId);
//
//    @GetMapping("/hello")
//    public String hello(@RequestParam(value = "name") String name);
//}
