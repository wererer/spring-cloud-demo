package com.emotte.configserver.filters;


import com.emotte.configserver.configuration.CustometRequestWrapper;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * <p> Description: 类描述</p>
 *
 * @Project eureka-server02
 * @Author: TF
 * @CreateDate: 2019/6/12 17:25
 * @UpdateUser: TF
 * @UpdateDate: 2019/6/12 17:25
 * @Version: 版本
 */
@WebFilter(filterName = "bodyFilter",urlPatterns = "/*")
@Order(1)
public class MyFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String url = new String(httpServletRequest.getRequestURI());
        //只过滤/acuator/bus-refresh请求
        if(!url.endsWith("/bus-refresh")){
            chain.doFilter(request,response);
            return;
        }
        //使用httpservletRequest保装原始请求达到修改post请求中body内容的目的
        CustometRequestWrapper requestWrapper = new CustometRequestWrapper(httpServletRequest);
        chain.doFilter(requestWrapper,response);
    }

    @Override
    public void destroy() {

    }
}
